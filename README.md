# Week4 mini Project

This project involves containerizing a simple Rust Actix web application using Docker.

## Start
1. Use `cargo new week4-mini-project` to build a web application

2. write Dokcerfile to enable docker

3. run `docker build -t week4 .` to build

4. run `docker run -p 8080:8080 week4` to start, visit localhost:8080 for web page.
![](1.png)
![](2.png)
![](3.png)
